import os

PG_CONFIG = {
    "database": "postgres",
    "user": "postgres",
    "password": "postgres",
    "host": "127.0.0.1",
    "port": "5432"
}

S3_CONFIG = {
    "endpoint_url": "https://storage.yandexcloud.net",
    "aws_access_key_id": os.getenv("S3_KEY"),
    "aws_secret_access_key": os.getenv("S3_ACCESS_KEY"),
    "bucket": os.getenv("S3_BUCKET")
}